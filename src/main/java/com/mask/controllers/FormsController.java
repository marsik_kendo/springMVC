package com.mask.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class FormsController {

    @Autowired
    public ApplicationContext applicationContext;

    // http://localhost:8189/app/showSimpleForm
    @RequestMapping("/showSimpleForm")
    public String showSimpleForm() {
        return "myform";
    }
    
//    @RequestMapping(path="/processForm", method=RequestMethod.GET)
//    public String processForm() {
//        return "myform-result";
//    }

//    @RequestMapping(path="/advProcessForm", method=RequestMethod.GET)
//    public String advProcessForm(@RequestParam("studentName") String studName, Model model) {
//        System.out.println(studName);
//        studName = "Student123: " + studName + " qwe123";
//        model.addAttribute("attrStudName", studName);
//        return "myform-result-adv";
//    }
    
    @RequestMapping(path="/advProcessForm", method=RequestMethod.GET)
    public String advProcessForm(HttpServletRequest request, Model model ) {
        String studName = request.getParameter("studentName");

        studName = "Student: " + studName;
        model.addAttribute("attrStudName", studName);
        return "myform-result-adv";
    }
}
