package com.mask.controllers;

import com.mask.entities.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@Controller
public class MainController {

    // http request methods - GET POST
    //http://localhost:8189/app/
    @RequestMapping(value = "/", method = RequestMethod.GET )
    public String showHomePage() {

        return "index";
    }

    @GetMapping("/info")
    public String showInfoPage(Model model) {
        String msg = "Java123edsaf";
        Student bob = new Student();
        bob.setFirstName("Bob123");

        model.addAttribute("message", msg);

        model.addAttribute("studentBob", bob);

        return "info";
    }
}
