<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/main.css" />
    </head>

    <body>
        <h1>Welcome page</h1>
        <h2>Index:</h2>
        <br>
        <a href="http://localhost:8190/app/showSimpleForm">Show Simple Form Page</a>
<%--        <a href="#">Show Simple Form Page</a>--%>
        <br>
        <a href="${pageContext.request.contextPath}/students/showForm">Show Students Form Page</a>
<%--        <a href="#">Show Students Form Page</a>--%>
    </body>
</html>