package com.mask.controllers;

import com.mask.entities.Student;
import com.mask.services.StudentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

//http://localhost:8189/app/students
@Controller
@RequestMapping("/students")
public class StudentsController {
    private StudentsService studentsService;

    @Autowired
    public void setStudentsService(StudentsService studentsService) {
        this.studentsService = studentsService;
    }

    // http://localhost:8189/app/students/showForm
    @RequestMapping("/showForm")
    public String showSimpleForm(Model model) {
        Student student = new Student();
        student.setId(100L);
        model.addAttribute("student", student);
        return "student-form";
    }

    // http://localhost:8189/app/students/processForm
    @PostMapping("/processForm")
    public String processForm(@ModelAttribute("student") Student student) {
        System.out.println(student.getFirstName() + " " + student.getLastName());
        return "student-form-result";
    }
    
    // http://localhost:8189/app/students/showStudentById?id=5
    @RequestMapping(path="/showStudentById", method=RequestMethod.GET)
    public String showStudentById(Model model, @RequestParam Long id) {
        Student student = studentsService.getStudentById(id);
        model.addAttribute("student", student);
        return "student-form-result";
    }

    // http://localhost:8189/app/students/getStudentById?id=20
    @GetMapping(path="/getStudentById")
    @ResponseBody
    //JSON
    // {
     //   "firstName":"Bob1",
    //    "lastName":"Ivanov",
    //     "country": "RU",
    //     "programmingLanguages" : [ "JAVA", "C++"],
    //     "cat" :  { "name" : "barsik", "age": 14}
    // }
    public Student getStudentById(@RequestParam Long id) {
        Student student = studentsService.getStudentById(id);
        return student;
    }



    // http://localhost:8189/app/students/getStudentById/10/qwe
    @GetMapping(path="/getStudentById/{qwe}/qwe")
    @ResponseBody
    public Student getStudentByIdFromPath(@PathVariable("qwe") Long id) {
        Student student = studentsService.getStudentById(id);
        return student;
    }
}
